package dominique.salaun.agenceimobiliere;

import dominique.salaun.agenceimobiliere.controller.BienImmobilierController;
import dominique.salaun.agenceimobiliere.entity.BienImmobilierEntity;
import dominique.salaun.agenceimobiliere.enums.TypeBienImmobilier;
import dominique.salaun.agenceimobiliere.repository.BienImmobilierRepository;
import dominique.salaun.agenceimobiliere.service.BienImmobilierService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class AgenceimobiliereApplicationTests {


	@Mock
	private BienImmobilierRepository bienImmobilierRepository;
	@InjectMocks
	private BienImmobilierController bienImmobilierController;



	@Test
	@Transactional
	public void TestGetAllBienImmobilier() {
		// Créer une liste simulée de biens immobiliers
		List<BienImmobilierEntity> simulatedBienImmobiliers = new ArrayList<>();

		BienImmobilierEntity bienImmobilier1 = new BienImmobilierEntity();
		bienImmobilier1.setIdBienImmobilier(1L);
		bienImmobilier1.setRue("123 Rue de la Liberté");
		bienImmobilier1.setCodePostal(75001);
		bienImmobilier1.setVille("Paris");


		BienImmobilierEntity bienImmobilier2 = new BienImmobilierEntity();
		bienImmobilier2.setIdBienImmobilier(2L);
		bienImmobilier2.setRue("456 Avenue des Champs-Élysées");
		bienImmobilier2.setCodePostal(75008);
		bienImmobilier2.setVille("Paris");


		simulatedBienImmobiliers.add(bienImmobilier1);
		simulatedBienImmobiliers.add(bienImmobilier2);


		// Définir le comportement attendu lors de l'appel de findAll dans le mock repository
		when(bienImmobilierRepository.findAll()).thenReturn(simulatedBienImmobiliers);

		// Appeler la méthode à tester
		ResponseEntity<List<BienImmobilierEntity>> result = bienImmobilierController.getAllBienImmobilier();

		// Vérifier que la méthode renvoie la liste simulée
		assertEquals(simulatedBienImmobiliers, result.getBody());
	}


	@Test
	@Transactional
	public void testGetBienImmobilier() {
		// Créer un ID simulé
		Long simulatedId = 1L;

		// Créer un bien immobilier simulé
		BienImmobilierEntity simulatedBienImmobilier = new BienImmobilierEntity();
		simulatedBienImmobilier.setIdBienImmobilier(simulatedId);
		simulatedBienImmobilier.setRue("123 Rue de la Liberté");
		simulatedBienImmobilier.setCodePostal(75001);
		simulatedBienImmobilier.setVille("Paris");
		simulatedBienImmobilier.setTypeBienImmobilier(TypeBienImmobilier.APPARTEMENT);


		// Cas où un bien immobilier est trouvé avec l'ID donné
		when(bienImmobilierRepository.findById(simulatedId)).thenReturn(Optional.of(simulatedBienImmobilier));
		ResponseEntity<?> resultFound = bienImmobilierController.getBienImmobilier(simulatedId);
		assertEquals(HttpStatus.OK, resultFound.getStatusCode());

		// Cas où aucun bien immobilier n'est trouvé avec l'ID donné
		when(bienImmobilierRepository.findById(simulatedId)).thenReturn(Optional.empty());
		ResponseEntity<?> resultNotFound = bienImmobilierController.getBienImmobilier(simulatedId);
		assertEquals(HttpStatus.NOT_FOUND, resultNotFound.getStatusCode());
	}





}
