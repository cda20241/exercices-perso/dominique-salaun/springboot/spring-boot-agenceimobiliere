package dominique.salaun.agenceimobiliere.Form;

/**
 * Cette classe représente un formulaire de saisie d'informations personnelles.
 */
public class PersonForm {
    private String firstName; // Prénom de la personne
    private String lastName;  // Nom de famille de la personne

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}