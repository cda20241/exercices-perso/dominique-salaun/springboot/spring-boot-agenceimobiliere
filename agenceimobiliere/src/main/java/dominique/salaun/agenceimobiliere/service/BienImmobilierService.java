package dominique.salaun.agenceimobiliere.service;

import dominique.salaun.agenceimobiliere.entity.BienImmobilierEntity;
import dominique.salaun.agenceimobiliere.repository.BienImmobilierRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
@RequiredArgsConstructor
public class BienImmobilierService {


    // Repository pour les opérations liées aux bienImmobilier
    private final BienImmobilierRepository bienImmobilierRepository;

    /**

     * Supprime une bienImmobilier en utilisant son identifiant.
     *
     * @param id L'identifiant du bienImmobilier à supprimer.
     * @return ResponseEntity avec un message approprié et le statut HTTP correspondant.
     */
    public ResponseEntity<String> bienImmobilierDelete(Long id) {
        // Recherche de bienImmobilier par son identifiant
        Optional<BienImmobilierEntity> bienImmobilierEntity = bienImmobilierRepository.findById(id);

        // Vérifie si le bienImmobilier existe
        if (bienImmobilierEntity.isPresent()) {
            // Supprime le bienImmobilier
            bienImmobilierRepository.deleteById(id);
            // Retourne une réponse avec le message de succès et le statut OK
            return new ResponseEntity<>("Le bienImmobilier est supprimée", HttpStatus.OK);
        } else {
            // BienImmobilier non trouvée, retourne une réponse avec un message approprié et le statut NOT_FOUND
            return new ResponseEntity<>("BienImmobilier non trouvée", HttpStatus.NOT_FOUND);
        }
    }

}
