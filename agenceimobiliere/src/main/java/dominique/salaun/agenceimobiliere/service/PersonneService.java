package dominique.salaun.agenceimobiliere.service;

import dominique.salaun.agenceimobiliere.entity.PersonneEntity;
import dominique.salaun.agenceimobiliere.repository.PersonneRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
@RequiredArgsConstructor
public class PersonneService {


    // Repository pour les opérations liées aux personne
    private final PersonneRepository personneRepository;

    /**

     * Supprime une personne en utilisant son identifiant.
     *
     * @param id L'identifiant du personne à supprimer.
     * @return ResponseEntity avec un message approprié et le statut HTTP correspondant.
     */
    public ResponseEntity<String> personneDelete(Long id) {
        // Recherche de personne par son identifiant
        Optional<PersonneEntity> personneEntity = personneRepository.findById(id);

        // Vérifie si le personne existe
        if (personneEntity.isPresent()) {
            // Supprime le personne
            personneRepository.deleteById(id);
            // Retourne une réponse avec le message de succès et le statut OK
            return new ResponseEntity<>("La personne est supprimée", HttpStatus.OK);
        } else {
            // Personne non trouvée, retourne une réponse avec un message approprié et le statut NOT_FOUND
            return new ResponseEntity<>("Personne non trouvée", HttpStatus.NOT_FOUND);
        }
    }

}
