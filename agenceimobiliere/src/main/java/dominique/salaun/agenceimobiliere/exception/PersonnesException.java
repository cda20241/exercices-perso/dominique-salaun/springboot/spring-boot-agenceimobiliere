package dominique.salaun.agenceimobiliere.exception;

/**
 * Cette classe représente une exception spécifique liée aux opérations sur les entités de personne dans le système d'agence immobilière.
 */
public class PersonnesException {
}