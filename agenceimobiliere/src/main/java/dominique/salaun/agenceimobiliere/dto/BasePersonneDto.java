package dominique.salaun.agenceimobiliere.dto;

import dominique.salaun.agenceimobiliere.entity.PersonneEntity;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class BasePersonneDto {


    private Long id;
    private String nom;
    private String prenom;

    public void mapper(PersonneEntity personne){
        this.id = personne.getIdPersonne();
        this.nom = personne.getNom();
        this.prenom = personne.getPrenom();

    }



}
