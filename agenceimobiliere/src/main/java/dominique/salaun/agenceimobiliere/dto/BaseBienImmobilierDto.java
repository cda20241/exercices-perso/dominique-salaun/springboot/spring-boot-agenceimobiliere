package dominique.salaun.agenceimobiliere.dto;

import dominique.salaun.agenceimobiliere.entity.BienImmobilierEntity;
import dominique.salaun.agenceimobiliere.enums.TypeBienImmobilier;
import jakarta.persistence.EnumType;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor
public class BaseBienImmobilierDto {
        private Long id;
        private String rue;
        private Integer code_postal;
        private String ville;
        private TypeBienImmobilier typeBien;
        private Integer nbrBiensPosseder;

    public void mapper(BienImmobilierEntity bienImmobilier) {
        this.id = bienImmobilier.getIdBienImmobilier();
        this.rue = bienImmobilier.getRue();
        this.code_postal = bienImmobilier.getCodePostal();
        this.ville = bienImmobilier.getVille();
        this.typeBien = bienImmobilier.getTypeBienImmobilier();
        this.calculNbrposseder(bienImmobilier);
    }


    private void calculNbrposseder(BienImmobilierEntity bienImmobilier) {
        this.nbrBiensPosseder = bienImmobilier.getListPersonneEntities().size();
    }


    public void mapper(List<BienImmobilierEntity> proprietaire) {
    }
}
