package dominique.salaun.agenceimobiliere.dto;

import dominique.salaun.agenceimobiliere.entity.BienImmobilierEntity;
import dominique.salaun.agenceimobiliere.entity.PersonneEntity;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@RequiredArgsConstructor
public class BienImmobilierDto extends BaseBienImmobilierDto{
    private List<BasePersonneDto> joueurs;


    public void mapper(BienImmobilierEntity bienImmobilier) {

        super.mapper(bienImmobilier);
        this.joueurs = new ArrayList<>();


        for (PersonneEntity personneEntity : bienImmobilier.getListPersonneEntities()) {
            BasePersonneDto dto = new BasePersonneDto();
            dto.mapper(personneEntity);
            this.joueurs.add(dto);
        }

    }



}
