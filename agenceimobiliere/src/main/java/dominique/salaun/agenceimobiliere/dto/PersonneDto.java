package dominique.salaun.agenceimobiliere.dto;

import dominique.salaun.agenceimobiliere.entity.PersonneEntity;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class PersonneDto extends BasePersonneDto{

    private BaseBienImmobilierDto bienImmobilier;

    public void mapper(PersonneEntity personneEntity) {
        super.mapper(personneEntity);
        this.bienImmobilier = new BaseBienImmobilierDto();
        this.bienImmobilier.mapper(personneEntity.getProprietes());
    }



}
