package dominique.salaun.agenceimobiliere.enums;


import lombok.Getter;
import lombok.RequiredArgsConstructor;


/**
 * Enumération représentant les types de biens immobiliers.
 */
@Getter
@RequiredArgsConstructor
public enum TypeBienImmobilier {
    APPARTEMENT, // Type: Appartement
    IMMEUBLE,    // Type: Immeuble
    MAISON;      // Type: Maison
}