package dominique.salaun.agenceimobiliere.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Enumération représentant les différents types de personnes liées à l'agence immobilière.
 * Chaque type de personne a une valeur associée.
 * Les valeurs sont définies en tant que constantes de l'énumération et ne peuvent pas être modifiées après l'initialisation.
 * Utilisation des annotations Lombok @Getter et @RequiredArgsConstructor pour générer automatiquement les accesseurs et le constructeur requis.
 */
@Getter
@RequiredArgsConstructor
public enum TypeRole {

    /**
     * Représente un agent immobilier.
     */
    AGENT_IMMOBILIER,

    /**
     * Représente un prospect.
     */
    PROSPECT,

    /**
     * Représente un client.
     */
    CLIENT,

    /**
     * Représente un propriétaire-vendeur.
     */
    PROPRIETAIRE_VENDEUR;





}
