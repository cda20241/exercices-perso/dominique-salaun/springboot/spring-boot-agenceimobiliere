package dominique.salaun.agenceimobiliere.Auth;

import dominique.salaun.agenceimobiliere.enums.TypeRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Requête d'enregistrement d'un nouvel utilisateur, contenant les informations telles que l'email, le mot de passe et le rôle.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {

    /**
     * L'adresse email de l'utilisateur à enregistrer.
     */
    private String email;

    /**
     * Le mot de passe de l'utilisateur à enregistrer.
     */
    private String password;

    /**
     * Le rôle associé à l'utilisateur à enregistrer.
     */
    private TypeRole role;
}