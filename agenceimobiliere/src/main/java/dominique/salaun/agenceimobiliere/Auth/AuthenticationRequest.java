package dominique.salaun.agenceimobiliere.Auth;


import dominique.salaun.agenceimobiliere.enums.TypeRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Requête d'authentification contenant l'e-mail et le mot de passe de l'utilisateur.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationRequest {

    private String email; // L'e-mail de l'utilisateur
    private String password; // Le mot de passe de l'utilisateur
}
