package dominique.salaun.agenceimobiliere.Auth;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * Contrôleur pour gérer les opérations d'authentification, telles que l'enregistrement et l'authentification des utilisateurs.
 */
@RestController
@RequestMapping("/Immobilier/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    /**
     * Enregistrer un nouvel utilisateur.
     *
     * @param request Les informations d'enregistrement de l'utilisateur.
     * @return Un ResponseEntity contenant la réponse d'authentification pour l'enregistrement.
     */
    @PostMapping("/register")
    public ResponseEntity<AuthentificationResponse> register(@RequestBody RegisterRequest request) {
        return ResponseEntity.ok(authenticationService.register(request));
    }

    /**
     * Authentifier un utilisateur.
     *
     * @param request Les informations d'authentification de l'utilisateur.
     * @return Un ResponseEntity contenant la réponse d'authentification pour l'authentification.
     */
    @PostMapping("/authenticate")
    public ResponseEntity<AuthentificationResponse> authenticate (@RequestBody AuthenticationRequest request) {
        return ResponseEntity.ok(authenticationService.authenticate(request));
    }
}