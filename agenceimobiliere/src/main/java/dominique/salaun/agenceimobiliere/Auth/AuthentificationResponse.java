package dominique.salaun.agenceimobiliere.Auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Réponse d'authentification renvoyée au client, contenant un message et un token.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthentificationResponse {

    /**
     * Le message associé à la réponse d'authentification.
     */
    private String message;

    /**
     * Le token généré pour l'authentification.
     */
    private String token;

}