package dominique.salaun.agenceimobiliere.Auth;

import dominique.salaun.agenceimobiliere.Conf.JwtService;
import dominique.salaun.agenceimobiliere.entity.PersonneEntity;
import dominique.salaun.agenceimobiliere.repository.PersonneRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Service pour gérer les opérations d'authentification, telles que l'enregistrement et l'authentification des utilisateurs.
 */
@Service
@RequiredArgsConstructor
public class AuthenticationService {

    // injection du repository de la classe User
    private final PersonneRepository personneRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    /**
     * Enregistre un nouvel utilisateur avec les informations fournies dans la requête d'enregistrement.
     *
     * @param request La requête d'enregistrement contenant les informations de l'utilisateur
     * @return Une réponse d'authentification avec un message et un jeton JWT généré avec succès
     */
    public AuthentificationResponse register(RegisterRequest request) {
        var personne = PersonneEntity.builder()
                .email(request.getEmail()) // Récupérer l'e-mail à partir de la requête
                .password(passwordEncoder.encode(request.getPassword())) // Crypter le mot de passe à partir de la requête
                .role(request.getRole()) // Récupérer le rôle à partir de la requête
                .build();
        personneRepository.save(personne); // Enregistrer l'utilisateur dans la base de données
        var jwtToken = jwtService.generateToken(personne); // Générer un jeton JWT pour l'utilisateur enregistré
        var msg= "Authentification reussie,Token Genereé avec succés";
        return AuthentificationResponse.builder()
                .message(msg) // Retourner un message de réussite
                .token(jwtToken) // Retourner le jeton JWT généré
                .build();
    }

    /**
     * Authentifie l'utilisateur avec les informations fournies dans la requête d'authentification.
     *
     * @param request La requête d'authentification contenant les informations de l'utilisateur
     * @return Une réponse d'authentification avec un jeton JWT généré avec succès
     */
    public AuthentificationResponse authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                // génération du token d'authentification de type username and password
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(), // Récupérer l'e-mail à partir de la requête
                        request.getPassword() // Récupérer le mot de passe à partir de la requête
                )
        );
        var personne = personneRepository.findUserByEmail(request.getEmail()) // Rechercher l'utilisateur par e-mail dans la base de données
                .orElseThrow(); // Lever une exception si l'utilisateur n'est pas trouvé
        var jwtToken = jwtService.generateToken(personne); // Générer un jeton JWT pour l'utilisateur authentifié
        return AuthentificationResponse.builder()
                .token(jwtToken) // Retourner le jeton JWT généré
                .build();
    }
}