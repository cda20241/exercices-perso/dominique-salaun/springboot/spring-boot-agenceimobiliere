package dominique.salaun.agenceimobiliere.Conf;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Classe de configuration pour les paramètres de sécurité, y compris l'authentification JWT et les règles d'autorisation.
 */
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {

    private final JwtAuthentificationFilter jwtAuthFilter;
    private final AuthenticationProvider authenticationProvider;

    /**
     * Configure la chaîne de filtres de sécurité pour les requêtes HTTP.
     *
     * @param http L'objet HttpSecurity à configurer
     * @return La SecurityFilterChain configurée
     */
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .csrf() // Désactive la protection contre les attaques CSRF
                .disable() // Désactive la protection contre les attaques CSRF
                .authorizeHttpRequests() // Autorise la configuration des règles d'autorisation
                .requestMatchers("/Immobilier/auth/register") // Les requêtes vers "/Immobilier/auth/register" sont autorisées sans authentification
                .permitAll() // Autorise toutes les requêtes vers "/Immobilier/auth/register" sans authentification
                .requestMatchers("/Immobilier/auth/authenticate") // Les requêtes vers "/Immobilier/auth/authenticate" sont autorisées sans authentification
                .permitAll() // Autorise toutes les requêtes vers "/Immobilier/auth/authenticate" sans authentification
                .anyRequest() // Pour toutes les autres requêtes
                .authenticated() // L'authentification est requise
                .and() // Termine la configuration actuelle et revient à la configuration parente
                .formLogin()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and() // Termine la configuration actuelle et revient à la configuration parente
                .authenticationProvider(authenticationProvider) // Utilise le fournisseur d'authentification spécifié
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class); // Ajoute le filtre d'authentification JWT avant le filtre par nom d'utilisateur et mot de passe

        return http.build(); // Retourne la chaîne de filtres configurée
    }
}