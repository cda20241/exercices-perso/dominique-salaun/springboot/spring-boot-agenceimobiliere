package dominique.salaun.agenceimobiliere.Conf;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Service pour la gestion des tokens JWT (JSON Web Tokens).
 */
@Service
public class JwtService {

    // Clé secrète utilisée pour la signature des tokens
    private static final String SECRET_KEY = "404E635266556A586E3272357538782F413F4428472B4B6250645367566B5970";

    /**
     * Extrait le nom d'utilisateur à partir du token JWT.
     *
     * @param token Le token JWT
     * @return Le nom d'utilisateur extrait du token
     */
    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    /**
     * Extrait une réclamation spécifique du token JWT en utilisant un résolveur de réclamation.
     *
     * @param token          Le token JWT
     * @param claimsResolver Le résolveur de réclamation
     * @param <T>            Le type de réclamation à extraire
     * @return La réclamation extraite du token
     */
    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    /**
     * Génère un jeton JWT pour les détails de l'utilisateur spécifié sans revendications supplémentaires.
     *
     * @param userDetails Les détails de l'utilisateur pour lesquels le jeton est généré
     * @return Le jeton JWT généré
     */
    public String generateToken(UserDetails userDetails) {
        return generateToken(new HashMap<>(), userDetails);
    }


    /**
     * Génère un nouveau token JWT pour l'utilisateur donné avec des réclamations supplémentaires.
     *
     * @param extraClaims    Les réclamations supplémentaires à inclure dans le token
     * @param userDetails    Les détails de l'utilisateur pour lesquels le token est généré
     * @return Le token JWT généré
     */
    public String generateToken(Map<String, Object> extraClaims, UserDetails userDetails) {
        return Jwts
                .builder()
                .setClaims(extraClaims)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 24)) // Token valide pendant 24 heures
                .signWith(getSignInKey(), SignatureAlgorithm.HS256)
                .compact();
    }

    /**
     * Vérifie si le token JWT donné est valide pour l'utilisateur donné.
     *
     * @param token        Le token JWT à vérifier
     * @param userDetails Les détails de l'utilisateur à comparer avec le token
     * @return true si le token est valide pour l'utilisateur donné, sinon false
     */
    public boolean isTokenValid(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername())) && !isTokenExpired(token);
    }

    /**
     * Vérifie si le token JWT donné a expiré.
     *
     * @param token Le token JWT à vérifier
     * @return true si le token a expiré, sinon false
     */
    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    /**
     * Extrait la date d'expiration du token JWT.
     *
     * @param token Le token JWT
     * @return La date d'expiration extraite du token
     */
    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    /**
     * Extrait toutes les réclamations du token JWT.
     *
     * @param token Le token JWT
     * @return Les réclamations extraites du token
     */
    private Claims extractAllClaims(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getSignInKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    /**
     * Récupère la clé de signature utilisée pour générer et valider les tokens JWT.
     *
     * @return La clé de signature
     */
    private Key getSignInKey() {
        byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(keyBytes);
    }
}
