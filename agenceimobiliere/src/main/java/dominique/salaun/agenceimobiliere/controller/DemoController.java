package dominique.salaun.agenceimobiliere.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/Immobilier/auth/demo-controller")
public class DemoController {

    /**
     * Point de terminaison pour renvoyer un message de salutation en fonction du rôle de l'utilisateur.
     *
     * @return Un ResponseEntity contenant le message de salutation approprié.
     */
    @GetMapping
    public ResponseEntity<String> sayHello() {
        // Obtenir l'authentification de l'utilisateur actuel
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        // Vérifier le rôle de l'utilisateur et renvoyer un message approprié
        if (authentication != null && authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_CLIENT"))) {
            return ResponseEntity.ok("Bonjour du point de terminaison sécurisé Client");
        } else if (authentication != null && authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_AGENT"))) {
            return ResponseEntity.ok("Bonjour du point de terminaison sécurisé Agent");
        } else if (authentication != null && authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_PROPRIETAIRE"))) {
            return ResponseEntity.ok("Bonjour du point de terminaison sécurisé Propriétaire");
        } else if (authentication != null && authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_PROSPECT"))) {
            return ResponseEntity.ok("Bonjour du point de terminaison sécurisé Prospect");
        } else {
            // Renvoyer un message par défaut si le rôle n'est pas reconnu
            return ResponseEntity.ok("Bonjour du point de terminaison sécurisé");
        }
    }
}