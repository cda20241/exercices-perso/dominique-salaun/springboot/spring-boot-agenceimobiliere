package dominique.salaun.agenceimobiliere.controller;

import dominique.salaun.agenceimobiliere.entity.PersonneEntity;
import dominique.salaun.agenceimobiliere.enums.TypeRole;
import dominique.salaun.agenceimobiliere.repository.PersonneRepository;
import dominique.salaun.agenceimobiliere.service.PersonneService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Contrôleur pour gérer les opérations liées aux personnes, telles que la récupération, l'ajout, la mise à jour et la suppression.
 */
@RestController
@RequestMapping("/Immobilier/auth/personne")
@RequiredArgsConstructor
public class PersonneController {

    private final PersonneRepository personneRepository;
    private final PersonneService personneService;

    /**
     * Récupérer toutes les personnes.
     *
     * @return Un ResponseEntity contenant la liste de toutes les personnes.
     */
    @GetMapping("/all")
    public ResponseEntity<List<PersonneEntity>> getAllPersonne() {
        List<PersonneEntity> personnes = personneRepository.findAll();
        return ResponseEntity.ok(personnes);
    }

    /**
     * Récupérer une personne par son ID.
     *
     * @param id L'ID de la personne à récupérer.
     * @return Un ResponseEntity contenant la personne correspondante, ou une réponse 404 si la personne n'est pas trouvée.
     */
    @GetMapping("/{id}")
    public ResponseEntity<?> getPersonne(@PathVariable Long id) {
        Optional<PersonneEntity> personne = personneRepository.findById(id);
        return personne.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Ajouter une nouvelle personne.
     *
     * @param personne La personne à ajouter.
     * @return Un ResponseEntity contenant la personne ajoutée.
     */
    @PostMapping("")
    public ResponseEntity<?> addPersonne(@RequestBody PersonneEntity personne) {
        personne.setIdPersonne(null);
        PersonneEntity savedPersonne = personneRepository.save(personne);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedPersonne);
    }

    /**
     * Ajouter plusieurs personnes.
     *
     * @param personnes La liste de personnes à ajouter.
     * @return Un ResponseEntity contenant la liste des personnes ajoutées.
     */
    @PostMapping("/addPersonnes")
    public ResponseEntity<List<?>> addPersonnes(@RequestBody List<PersonneEntity> personnes) {
        List<PersonneEntity> savedPersonnes = personneRepository.saveAll(personnes);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedPersonnes);
    }

    /**
     * Mettre à jour une personne existante.
     *
     * @param id L'ID de la personne à mettre à jour.
     * @param personne La nouvelle version de la personne.
     * @return Un ResponseEntity contenant la personne mise à jour, ou une réponse 404 si l'ID n'existe pas.
     */
    @PutMapping("/{id}")
    public ResponseEntity<?> updatePersonne(@PathVariable Long id, @RequestBody PersonneEntity personne) {
        if (personneRepository.existsById(id)) {
            personne.setIdPersonne(id);
            PersonneEntity updatedPersonne = personneRepository.save(personne);
            return ResponseEntity.ok(updatedPersonne);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("L'ID spécifié n'existe pas");
        }
    }

    /**
     * Supprimer une personne par son ID.
     *
     * @param id L'ID de la personne à supprimer.
     * @return Un ResponseEntity contenant un message indiquant le succès de la suppression.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePersonne(@PathVariable Long id) {
        return personneService.personneDelete(id);
    }

    /**
     * Récupérer les personnes par type de rôle.
     *
     * @param role Le rôle pour lequel récupérer les personnes.
     * @return Un ResponseEntity contenant la liste des personnes correspondant au rôle spécifié.
     */
    @GetMapping("/byType/{role}")
    public ResponseEntity<List<PersonneEntity>> getPersonnesByRole(@PathVariable String role) {
        TypeRole typeRole = TypeRole.valueOf(role.toUpperCase());
        List<PersonneEntity> personnesByRole = personneRepository.findByRole(typeRole);
        return ResponseEntity.ok(personnesByRole);
    }
}