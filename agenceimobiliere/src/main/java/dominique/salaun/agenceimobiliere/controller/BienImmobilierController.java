package dominique.salaun.agenceimobiliere.controller;

import dominique.salaun.agenceimobiliere.entity.BienImmobilierEntity;
import dominique.salaun.agenceimobiliere.enums.TypeBienImmobilier;
import dominique.salaun.agenceimobiliere.repository.BienImmobilierRepository;
import dominique.salaun.agenceimobiliere.service.BienImmobilierService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Contrôleur pour la gestion des biens immobiliers.
 */
@RestController
@RequestMapping("/Immobilier/auth/bienImmobilier")
@RequiredArgsConstructor
public class BienImmobilierController {

    private final BienImmobilierRepository bienImmobilierRepository;
    private final BienImmobilierService bienImmobilierService;

    /**
     * Obtenir tous les biens immobiliers.
     *
     * @return Liste de tous les biens immobiliers
     */
    @GetMapping("/all")
    public ResponseEntity<List<BienImmobilierEntity>> getAllBienImmobilier() {
        List<BienImmobilierEntity> bienImmobiliers = bienImmobilierRepository.findAll();
        return ResponseEntity.ok(bienImmobiliers);
    }

    /**
     * Obtenir un bien immobilier par ID.
     *
     * @param id L'ID du bien immobilier
     * @return Le bien immobilier s'il est trouvé, ou 404 s'il n'est pas trouvé
     */
    @GetMapping("/{id}")
    public ResponseEntity<?> getBienImmobilier(@PathVariable Long id) {
        try {
            Optional<BienImmobilierEntity> bienImmobilier = bienImmobilierRepository.findById(id);
            return bienImmobilier.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur est survenue lors de la récupération du bien immobilier : " + e.getMessage());
        }
    }

    // ... (autres méthodes omises pour des raisons de concision)

    /**
     * Obtenir des biens immobiliers par type.
     *
     * @param type Le type de bien immobilier
     * @return Liste des biens immobiliers du type spécifié
     */
    @GetMapping("/byType/{type}")
    public ResponseEntity<List<BienImmobilierEntity>> getBienImmobiliersByType(@PathVariable String type) {
        TypeBienImmobilier typeBienImmobilier = TypeBienImmobilier.valueOf(type.toUpperCase());
        List<BienImmobilierEntity> bienImmobiliersByType = bienImmobilierRepository.findByTypeBienImmobilier(typeBienImmobilier);
        return ResponseEntity.ok(bienImmobiliersByType);
    }
}