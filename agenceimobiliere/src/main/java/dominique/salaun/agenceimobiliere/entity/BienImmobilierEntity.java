package dominique.salaun.agenceimobiliere.entity;

import dominique.salaun.agenceimobiliere.enums.TypeBienImmobilier;
import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe représente une entité de bien immobilier dans le système d'agence immobilière.
 */
@Data
@RequiredArgsConstructor
@Entity(name = "Bien_Immobilier")
public class BienImmobilierEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_BienImmobilier")
    private Long idBienImmobilier;

    // Rue de l'adresse du bien immobilier
    private String rue;

    // Code postal de l'adresse du bien immobilier
    private Integer codePostal;

    // Ville de l'adresse du bien immobilier
    private String ville;

    // Type de bien immobilier
    @Enumerated(EnumType.STRING)
    private TypeBienImmobilier typeBienImmobilier;

    // Propriétaire du bien immobilier
    @ManyToOne
    @JoinColumn(name = "proprietaire_id")
    private PersonneEntity proprietaire;

    // Agent immobilier responsable du bien immobilier
    @ManyToOne
    @JoinColumn(name = "agent_immobilier_id")
    private PersonneEntity agentImmobilier;

    // Liste de personnes associées au bien immobilier
    @ManyToMany()
    @JoinTable(name = "Bien_au_Personne",
            joinColumns = @JoinColumn(name = "fk_BienImmobilier", referencedColumnName = "id_BienImmobilier"),
            inverseJoinColumns = @JoinColumn(name = "fk_Personne", referencedColumnName = "id_Personne"))
    private List<PersonneEntity> listPersonneEntities = new ArrayList<>();
}