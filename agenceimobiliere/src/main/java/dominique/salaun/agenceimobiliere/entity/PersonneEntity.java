package dominique.salaun.agenceimobiliere.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import dominique.salaun.agenceimobiliere.enums.TypeRole;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * Cette classe représente une entité personne dans le système d'agence immobilière.
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "Personne")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idPersonne")
public class PersonneEntity implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_Personne", nullable = false, updatable = false)
    private Long idPersonne;

    // Rôle de la personne
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private TypeRole role;

    // Nom de famille de la personne
    private String nom;

    // Email de la personne
    private String email;

    // Mot de passe de la personne
    private String password;

    // Prénom de la personne
    private String prenom;

    // Rue de l'adresse de la personne
    private String rue;

    // Code postal de l'adresse de la personne
    private String codePostal;

    // Ville de l'adresse de la personne
    private String ville;

    // Liste de propriétés possédées par la personne
    @ManyToMany(mappedBy = "listPersonneEntities")
    private List<BienImmobilierEntity> proprietes = new ArrayList<>();

    /**
     * Retourne la liste des rôles accordés à la personne.
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    /**
     * Retourne l'adresse email de la personne.
     */
    @Override
    public String getUsername() {
        return email;
    }

    /**
     * Indique si le compte de la personne n'a pas expiré.
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Indique si le compte de la personne n'est pas verrouillé.
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * Indique si les informations d'identification de la personne ne sont pas expirées.
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Indique si le compte de la personne est activé.
     */
    @Override
    public boolean isEnabled() {
        return true;
    }



}
