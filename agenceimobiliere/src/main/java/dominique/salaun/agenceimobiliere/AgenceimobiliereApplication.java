package dominique.salaun.agenceimobiliere;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Classe principale pour démarrer l'application Spring Boot.
 */
@SpringBootApplication
public class AgenceimobiliereApplication {

	/**
	 * Point d'entrée de l'application.
	 *
	 * @param args Les arguments de la ligne de commande (non utilisés dans cet exemple).
	 */

	public static void main(String[] args) {
		SpringApplication.run(AgenceimobiliereApplication.class, args);


		// Spécifiez l'URL Swagger
		String swaggerUrl = "http://localhost:8080/Immobilier/auth/swagger-ui/index.html";
		// Affiche un message dans le terminal avec le lien vers Swagger
		System.out.println("Ouvrez l'URL Swagger dans votre navigateur : " + swaggerUrl);
	}


	}
