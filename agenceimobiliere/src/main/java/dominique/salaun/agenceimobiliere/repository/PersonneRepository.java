package dominique.salaun.agenceimobiliere.repository;

import dominique.salaun.agenceimobiliere.entity.PersonneEntity;
import dominique.salaun.agenceimobiliere.enums.TypeRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Interface for accessing PersonneEntity objects from the database.
 */
public interface PersonneRepository extends JpaRepository<PersonneEntity, Long> {

    /**
     * Finds a list of PersonneEntity objects by role.
     *
     * @param role The role to search for
     * @return A list of PersonneEntity objects with the specified role
     */
    List<PersonneEntity> findByRole(TypeRole role);

    /**
     * Finds a PersonneEntity object by email.
     *
     * @param email The email to search for
     * @return An Optional containing the PersonneEntity with the specified email, or empty if not found
     */
    Optional<PersonneEntity> findUserByEmail(String email);
}