package dominique.salaun.agenceimobiliere.repository;

import dominique.salaun.agenceimobiliere.entity.BienImmobilierEntity;
import dominique.salaun.agenceimobiliere.enums.TypeBienImmobilier;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Interface représentant le repository pour l'entité "BienImmobilierEntity" dans le système d'agence immobilière.
 */
public interface BienImmobilierRepository extends JpaRepository<BienImmobilierEntity, Long> {
    /**
     * Recherche et retourne une liste de biens immobiliers en fonction du type de bien.
     * @param type Le type de bien immobilier
     * @return Liste de biens immobiliers correspondant au type spécifié
     */
    List<BienImmobilierEntity> findByTypeBienImmobilier(TypeBienImmobilier type);
}